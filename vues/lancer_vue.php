<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<div class="panneau">

  <div class="panneau_details">
    <!--Partie 1 du formulaire de création d'une nouvelle partie-->

	<h2>Paramètres d'une partie :</h2>



    <?php
        if($etape == 0){
    ?>
    <form class="bloc_commandes" method="post" action="#">	
        <div class="select-container">
            <!--Nb de cartes-->
            <label for="nbCartesPlateau">Nombre de carte sur le plateau</label>		
            <select name="nbCP" id="nbCP">
                <?php
                for ($i = 1; $i <= 20; $i++) {
                    echo "<option value=\"$i\">$i</option>";
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <!--Nb de cartes vertes-->
            <label for="nbCartesVertes">Nombre de cartes vertes</label>		
            <select name="nbCV" id="nbCV">
                <?php
                for ($i = 1; $i <= 20; $i++) {
                    echo "<option value=\"$i\">$i</option>";
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <!--Nb de cartes oranges-->
            <label for="nbCartesOrange">Nombre de cartes oranges</label>		
            <select name="nbCO" id="nbCO">
                <?php
                for ($i = 1; $i <= 20; $i++) {
                    echo "<option value=\"$i\">$i</option>";
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <!--Nb de noires-->
            <label for="nbCartesNoires">Nombre de cartes noires</label>		
            <select name="nbCN" id="nbCN">
                <?php
                for ($i = 1; $i <= 20; $i++) {
                    echo "<option value=\"$i\">$i</option>";
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <!--Nb de joueurs-->
            <label for="nombreJoueur">Nombre de joueurs</label>		
            <select name="nbJoueur" id="nbCN">
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    echo "<option value=\"$i\">$i</option>";
                }
                ?>
            </select>
        </div>
        <br><br>
        <div class="select-container">
            <input type="submit" name="form1" value="Suivant"/>
        </div>
        <p><?php echo $erreur_message; ?></p>
    </form>
    <?php

    }else if ($etape == 1){
    ?>
     <!--Partie 2 du formulaire de création d'une nouvelle partie s'affiche si la partie 1 a été validée-->
    <form class="bloc_commandes" method="post" action="#">	

        <?php 

        for ($i = 1; $i <= $_SESSION['nbJoueur']; $i++) {?>
        <div class="select-container">

            <label for=<?php echo "joueur_$i"?>> <?php echo "Joueur $i"?> </label>		
            <select name=<?php echo "joueur_$i"?> id=<?php echo "joueur_$i"?>>
                <?php
                    open_connection_DB();
                    $donnees = executer_une_requete("SELECT MAX(idJ) FROM `JOUEUR`");
                    $max_idJ = $donnees[0]['MAX(idJ)'];
                    

                    for ($j = 0; $j <= $max_idJ; $j++) {
                        if($j == 0){
                            echo "<option value='$j'>Séléctionner un joueur</option>";
                        }
                        else{
                            $donneesNom = executer_une_requete("SELECT nom FROM `JOUEUR` WHERE idJ = '$j'");
                            $nomJoueur = $donneesNom[0]['nom'];
    
                            $donneesPrenom = executer_une_requete("SELECT prénom FROM `JOUEUR` WHERE idJ = '$j'");
                            $prenomJoueur = $donneesPrenom[0]['prénom'];
    
                            $donneesPseudo = executer_une_requete("SELECT pseudo FROM `JOUEUR` WHERE idJ = '$j'");
                            $pseudoJoueur = $donneesPseudo[0]['pseudo'];
    
                            if (!empty($nomJoueur)) {
                                echo "<option value='$j'>$prenomJoueur $nomJoueur, $pseudoJoueur</option>";
                            }
                        }

                    }
                ?>
            </select>
        </div>
        <br><br>
        <?php } ?>


        <div class="select-container">
            <label for="Strategie">Qui commence ?</label>		
            <select name="strategie" id="strategie">
                <option disabled selected>Sélectionner une stratégie</option>
                <option value="+JEUNE">Honneur au plus jeune</option>
                <option value="-EXPE">Honneur au moins expérimenté</option>
                <option value="ALEA" selected>Aléatoire</option>
            </select>

        </div>
        <br><br>
        <div class="select-container">
            <input type="submit" name="form2" value="Suivant"/>
        </div>
        <p><?php echo $erreur_message; ?></p>
    </form>
    <?php
        } else{ // Redirection vers une autre page
            $donneeidCarte = executer_une_requete("SELECT DISTINCT idC FROM CARTE");
            $_SESSION['idc'] = $donneeidCarte[0]['idC'];
            header("Location: index.php?page=jouerPartie");
          } 
    ?>

</div>