<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<div class="cont">
<table class="info">
<?php
if (!empty($_SESSION['idPartie'])) {
    // Afficher les informations de la partie
    echo "<tr><th>Informations sur la partie</th></tr>\n";

    echo "<tr><td>ID de la partie : " . $_SESSION['idPartie'] . "</td></tr>";

    echo "<tr><td>Nombre de joueurs : " . $_SESSION['nbJoueur'] . " joueur(s)</td></tr>";

    echo "<tr><td>Mode de jeu : " . $_SESSION['mode'] . "</td></tr>";

    echo "<tr><td>Etat de la partie : " . $_SESSION['etat'] . "</td></tr>";

}
?>
</table>
<div class="pions">
<?php
// Vérification de la présence des données des joueurs
if (!empty($_SESSION['pseudo']) && !empty($_SESSION['pion'])) {
    echo "<div>";
    // Affichage de la liste des joueurs
    echo "<h2>Liste des joueurs</h2>";
    ?>
    <table class="players">
        <tr>
            <th>Pseudo</th>
            <th>idJ</th>
        </tr>
        <?php
        // Récupération des données des joueurs depuis la session
        $tabPseudo = $_SESSION['pseudo'];
        $tabId = $_SESSION['indice_joueur'];
        $tabScore = $_SESSION['score'];
        $tabPions = $_SESSION['pion'];
        $tabOrdre = $_SESSION['ordre'];
        // Boucle pour parcourir les joueurs
        for ($i = 0; $i < $_SESSION['nbJoueur']; $i++) {
            ?>
            <tr>
                <td><?php echo $tabPseudo[$tabId[$i + 1]]; ?></td>
                <td><?php echo $tabId[$i + 1]; ?></td>
                
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    // Affichage de l'ordre de passage
    echo "<h2>Ordre de passage</h2>";
    ?>
    <table class="players">
        <tr>
            <th>N°</th>
            <th>Pseudo</th>
            <th>idJ</th>
            <th>Score</th>
            <th>Pion</th>
        </tr>
        <?php
        // Boucle pour afficher l'ordre de passage
for ($i = 0; $i < $_SESSION['nbJoueur']; $i++) {
    ?>
    <tr>
        <td><?php echo $i + 1; ?></td>
        <td><?php echo $tabPseudo[$tabOrdre[$i]]; ?></td>
        <td><?php echo $tabOrdre[$i]; ?></td>
        <td><?php echo isset($tabScore[$tabOrdre[$i]]) && $tabScore[$tabOrdre[$i]] !== null ? $tabScore[$tabOrdre[$i]] : 0; ?></td>        <td><?php
                // Récupérer l'indice du joueur dans l'ordre actuel
                $indice_joueur = $tabOrdre[$i];

                // Afficher l'image du pion selon la couleur du joueur
                switch ($_SESSION['pion'][$indice_joueur]) {
                    case 'rouge':
                        echo "<img src='img/pRouge.png' alt='Pion rouge'>";
                        break;
                    case 'vert':
                        echo "<img src='img/pVert.png' alt='Pion vert'>";
                        break;
                    case 'bleu':
                        echo "<img src='img/pBleu.png' alt='Pion bleu'>";
                        break;
                    case 'jaune':
                        echo "<img src='img/pJaune.png' alt='Pion jaune'>";
                        break;
                    case 'orange':
                        echo "<img src='img/pOrange.png' alt='Pion orange'>";
                        break;
                    default:
                        echo "Pion non reconnu";
                }

                // Fermer la cellule pour le pion
                echo "</td>";
            }

            ?>
        </td>
    </tr>
    <?php
}
        ?>
    </table>
    </div>

</div>
</div>
</div>
</div>
<?php
$imageFolder = 'img/ressources_graphiques/';
$desfolder = 'éléments_graphiques/';
?>
<div class = "container">
<div class="Cartes">
<?php
// Affichage des images
$imagesC = array();
for ($i = 1; $i <= 75; $i++) {
    $imagesC[] = 'C' . str_pad($i, 3, '0', STR_PAD_LEFT) . '.png';
}
?>

<table class="tabJeu">
    <tr class="Tour">
    <?php echo "<th colspan='" . ($_SESSION['nbCP'] + 2) . "'>Le tour est à: " .  $tabPseudo[$_SESSION['jAct']] ."</th>";
    ?>
    </tr>
    <tr class="Tentaive">
    <?php echo "<th colspan='" . ($_SESSION['nbCP'] + 2) . "'>Tentative: " . $_SESSION['tentative'] . " /3</th>";
    ?>
    </tr>

    <tr class="cartesIMG">
        <?php
        $nombre_colonnes = $_SESSION['nbCP'];
        echo "<td><img src='$imageFolder/carteDépart.png' alt='Carte de Départ'></td>";
        
        // Générer les cellules de la deuxième ligne
        if (isset($_SESSION['deck']) && isset($_SESSION['deck']['cartes']) && is_array($_SESSION['deck']['cartes'])) {
            // Parcourir le tableau de cartes dans le deck pour afficher les images
            foreach ($_SESSION['deck']['cartes'] as $carte) {
                echo "<td>";
                echo "<img src='" . $imageFolder . $carte['img'] . "' alt='" . $carte['idC'] . "'>";
                echo "</td>";
            }
        } else {
            echo "Le deck de cartes ou le tableau de cartes est vide ou non défini.";
        }
        
        echo "<td><img src='$imageFolder/carteArrivée.png' alt='Carte d'arrivée'></td>";
        ?>
    </tr>
    <tr class="cartesID">
        <?php
        // Générer les cellules de la première ligne
        echo "<td>Carte de Départ</td>";
        for ($i = 1; $i <= $nombre_colonnes; $i++) {
            echo "<td>Carte $i</td>";
        }
        echo "<td>Carte d'Arrivée</td>";
        ?>
    </tr>

    <?php
// Récupérer les positions des pions
$positions_pions = $_SESSION['position'];
$tabPions = $_SESSION['pion'];

// Parcourir les colonnes et afficher les cellules des pions
for ($i = 0; $i <= $nombre_colonnes + 1; $i++) {
    // Commencer la cellule pour le pion
    echo "<td class='pionsIMG'>";

    // Vérifier s'il y a des joueurs à cette position
    $joueurs_position = array_filter($positions_pions, function($position) use ($i) {
        return $position == $i;
    });

    // Afficher les pions de tous les joueurs à cette position
    foreach ($joueurs_position as $idJoueur => $position) {
        // Afficher l'image du pion selon la couleur du joueur
        switch ($tabPions[$idJoueur]) {
            case 'rouge':
                echo "<img src='img/pRouge.png' alt='Pion rouge'>";
                break;
            case 'vert':
                echo "<img src='img/pVert.png' alt='Pion vert'>";
                break;
            case 'bleu':
                echo "<img src='img/pBleu.png' alt='Pion bleu'>";
                break;
            case 'jaune':
                echo "<img src='img/pJaune.png' alt='Pion jaune'>";
                break;
            case 'orange':
                echo "<img src='img/pOrange.png' alt='Pion orange'>";
                break;
            default:
                // Si la couleur du pion n'est pas reconnue, afficher un message d'erreur
                echo "Erreur: couleur de pion non reconnue";
        }
    }

    // Fermer la cellule pour le pion
    echo "</td>";
}
?>



</table>
</div>

<form class="choixMain" method="post" action="#">
    <table border="0">
        <caption><h2>Tour composé de:</h2></caption>
        <!-- Liste déroulante pour le nombre de dés rouges -->
        <tr>
            <td><h3>Dés rouges:</h3></td>
            <td>
                <select name="nbDesRouges">
                    <?php for ($i = 0; $i <= $_SESSION['nbDesRmax']; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <!-- Liste déroulante pour le nombre de dés bleus -->
        <tr>
            <td><h3>Dés bleus:</h3></td>
            <td>
                <select name="nbDesBleus">
                    <?php for ($i = 0; $i <= $_SESSION['nbDesBmax']; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <!-- Liste déroulante pour le nombre de dés jaunes -->
        <tr>
            <td><h3>Dés jaunes:</h3></td>
            <td>
                <select name="nbDesJaunes">
                    <?php for ($i = 0; $i <= $_SESSION['nbDesJmax']; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <!-- Bouton de soumission pour la sélection des dés -->
        <tr>
            <td colspan="2">
                <input type="submit" name="choixMain" value="Choisir main">
            </td>
        </tr>
    </table>
</form>
</div>


<form class="Affectation" method="post" action="#">
    <table border="0">
        <caption><h2>Dés obtenus:</h2></caption>
        
        <?php foreach ($_SESSION['faceSelect'] as $couleur => $faces) { ?>
            <!-- Dés <?php echo ucfirst($couleur); ?> -->
            <tr>
                <td colspan="2"><h3>Dés <?php echo ucfirst($couleur); ?>:</h3></td>
            </tr>
            <?php foreach ($faces as $i => $face) { ?>
                <tr>
                    <td>
                        <img src="<?php echo $imageFolder . $desfolder . '\de' . ucfirst($couleur[0]) . $face . '.png'; ?>" alt="Dé <?php echo ucfirst($couleur); ?> <?php echo $i; ?>">
                    </td>
                    <td>
                        <select name="CTT_<?php echo $i; ?>" id="CTT_<?php echo $i; ?>">
                            <option disabled selected>Affecter à une carte</option>
                            <?php for ($j = 1; $j <= $_SESSION['nbCP']; $j++) { ?>
                                <option value="<?php echo $j; ?>">Carte <?php echo $j; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        
        <!-- Bouton Avancer -->
        <tr>
            <td colspan="2">
                <input type="submit" name="RelancerDes" value="Relancer les dés">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="Avancer" value="Avancer">
            </td>
        </tr>
    </table>
</form>
</div>
<?php

//echo '<div class="debug">';
/*
// Mélanger les cartes du deck
shuffle($_SESSION['deck']);

// Afficher la ligne des cartes dans le tableau
echo "<tr class='cartesIMG'>";

if (isset($_SESSION['deck']['cartes']) && is_array($_SESSION['deck']['cartes']) && !empty($_SESSION['deck']['cartes'])) {
    // Parcourir le tableau de cartes dans le deck
    foreach ($_SESSION['deck']['cartes'] as $carte) {
        // Récupérer le chemin de l'image de la carte
        $cheminImage = $carte['img'];

        // Afficher l'image de la carte
        echo "<td><img src='$cheminImage' alt='Carte'></td>";
    }
} else {
    echo "Le deck de cartes est vide ou non défini.";
}


// Afficher la ligne des identifiants de cartes dans le tableau
echo "<tr class='cartesID'>";


if (isset($_SESSION['deck']['cartes']) && is_array($_SESSION['deck']['cartes']) && !empty($_SESSION['deck']['cartes'])) {
    // Parcourir le tableau de cartes dans le deck pour afficher les identifiants
    foreach ($_SESSION['deck']['cartes'] as $carte) {
        // Récupérer l'idC de la carte
        $idCarte = $carte['idC'];
        // Afficher l'identifiant de la carte
        echo "<td>Carte $idCarte</td>";
    }
} else {
    echo "Le deck de cartes est vide ou non défini.";
}


echo "</div>";
 // Fin de la section de débogage
*/
/*if (isset($_SESSION['deck']) && isset($_SESSION['deck']['cartes']) && is_array($_SESSION['deck']['cartes'])) {
    // Parcourir le tableau de cartes dans le deck pour afficher les images
    foreach ($_SESSION['deck']['cartes'] as $carte) {
        echo "<img src='" . $imageFolder . $carte['img'] . "' alt='" . $carte['idC'] . "'>";
        echo "</br>"; 
        var_dump($carte['img']);
    }
}*/

//echo "</div>";
?>