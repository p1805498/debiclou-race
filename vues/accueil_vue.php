<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<main>
    <div class="panneau">
        <div class = "panneau_details"> 
            <?php
                $etape = $_SESSION['etapeConnexion'];
                if($etape == 0){
            ?>
             <!--Formulaire de connexion si aucun user n'est connecté-->
            <div id="popupConnexion" class="connect">
                <form class="bloc_commandes" method="post" action="#">
                    <label for="username">Nom d'utilisateur :</label>
                    <select class="button" name=<?php echo "username"?> id=<?php echo "username"?>>
                        <?php
                            $donnees = executer_une_requete("SELECT MAX(idJ) FROM `JOUEUR`");
                            $max_idJ = $donnees[0]['MAX(idJ)'];
                            
                            // Parcours de la bdd pour récupérer les noms, prenoms des joueur
                            for ($j = 0; $j <= $max_idJ; $j++) {
                                if($j == 0){
                                    echo "<option value='$j'>Séléctionner un joueur</option>";
                                }
                                else{
                                    $donneesNom = executer_une_requete("SELECT nom FROM `JOUEUR` WHERE idJ = '$j'");
                                    $nomJoueur = $donneesNom[0]['nom'];
            
                                    $donneesPrenom = executer_une_requete("SELECT prénom FROM `JOUEUR` WHERE idJ = '$j'");
                                    $prenomJoueur = $donneesPrenom[0]['prénom'];
            
                                    $donneesPseudo = executer_une_requete("SELECT pseudo FROM `JOUEUR` WHERE idJ = '$j'");
                                    $pseudoJoueur = $donneesPseudo[0]['pseudo'];
            
                                    if (!empty($nomJoueur)) {
                                        echo "<option value='$j'>$prenomJoueur $nomJoueur, $pseudoJoueur</option>";
                                    }
                                }

                            }
                        ?>
                    </select>
                    <label for="password">Mot de passe :</label>
                    <input type="password" id="password" name="password" placeholder="Mot de passe = 12345">
                    <input type="submit" name="connect" value="Se connecter">
                </form>
                <p><?php echo $erreur_message; ?></p>
            </div>
            <div class="inscr">
                <form method="post" action="#">Pas encore de compte ?
                    <input type="submit" name="inscription" value="Inscription">
                </form>
            </div>
             <!--Description du site web-->
            <p class="bloc_commandes">Bonjour chers utilisateurs,<br>Vous vous trouvez sur le site web créer par Nizar BOUARROUDJ et Léon VANDEN BORRE, étudiants en Licence 2 Informatique à l'université Claude Bernard, Lyon 1.<br>Ce site a été réalisé dans le cadre de l'UE BDW dans un but éducatif dans pour apprendre à gérer les bases de données liés au web.<br>Ce site permet de jouer à un jeu de société numérisé qui contient une base de données dans laquelle sont gérés la création, la modification, la suppression et l'interrogation de tables.<br>Les règles du jeu et le fonctionnement des diverses fonctionnalités disponibles sont detaillés dans la section <a href="./index.php?page=regles">RÈGLES DU JEU</a></p>

            <?php
                }
                else if($etape == 1){
            ?>
             <!--Si l'user clique sur inscription, il a la possibilité d'enregistré un nouveau joueur en base et sera connecté directement-->
            <div class="inscription">
                <h2>Inscription :</h2>
                <form class="bloc_commandes" method="post" action="#">	
                    <div class="input-container">
                        <label for="nom">Nom :</label>		
                        <input type="text" name="nom" id="nom" placeholder="Entrez votre nom">
                    </div>
                    <br><br>
                    <div class="input-container">
                        <label for="prenom">Prénom :</label>		
                        <input type="text" name="prenom" id="prenom" placeholder="Entrez votre prénom">
                    </div>
                    <br><br>
                    <div class="input-container">
                        <label for="pseudo">Pseudo :</label>		
                        <input type="text" name="pseudo" id="pseudo" placeholder="Entrez votre pseudo">
                    </div>
                    <br><br>

                    <div class="input-container">
                        <label for="dateNaiss">Date de naissance :</label>		
                        <input type="date" name="dateNaiss" id="dateNaiss" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="YYYY-MM-DD">
                    </div>

                    <br><br>
                    <div class="input-container">
                        <label for="mail">Adresse mail :</label>		
                        <input type="text" name="mail" id="mail" placeholder="Entrez votre adresse mail">
                    </div>
                    <br><br>
                    <div class="select-container">
                        <input type="submit" name="disconnect" value="Annuler"/>
                        <input type="submit" name="newPlayer" value="Valider"/>
                    </div>
                    <p><?php echo $erreur_message; ?></p>
                </form>
            </div>

             <!--Si un user est connecté, on affiche le message de description ainsi que les tournois auxquels il est inscrit-->
            <?php
                }
                else {
                    
                    $prenomJoueur = $_SESSION['prenomJoueur'];
                    echo("<h3>Binvenue $prenomJoueur !</h3>");
                    echo("<p class=\"bloc_commandes\">Bonjour chers utilisateurs,<br>Vous vous retrouvez sur le site web créer par Nizar BOUARROUDJ & Leon VANDEN BORRE, étudiants en Licence 2 Informatique.<br>Ce site a été réalisé dans le cadre de l'UE BDW dans un but educatif dans pour apprendre à gerer les bases de données liés au web.<br>Ce site permet de jouer à un jeu de société numérisé qui contient une base de données dans laquelle on gerera: création, modification, suppression et interrogation de tables.<br>Les règles du jeu et le fonctionnement des divers fonctionnalités disponibles sont detaillés dans la section <a href=\"./index.php?page=regles\">REGLES DU JEU</a></p>");

                    phaseTournoi();
            ?>
            <form class="button" method="post" action="#">
                <input class="button" type="submit" name="disconnect" value="Se déconnecter">
            </form>
            <?php
                }
            ?>


            <div id="logo_cont"><img id="univ_logo" src="https://www.univ-lyon1.fr/uas/WWW-2014/LOGO/LogoUCBL1.png" alt="UCBL"/></div>
        </div>
    </div>
</main>
