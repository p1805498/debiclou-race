// un n-uplet contenant le nombre de joueurs, le nombre d’équipes, le nombre de classements, le nombre de
// tournois et la moyenne des participants par tournoi".
SELECT COUNT( DISTINCT J.idJ ), COUNT( DISTINCT E.idE ), COUNT( DISTINCT C.idC ), COUNT( DISTINCT T.idT), AVG( f )
FROM Joueur J
JOIN Equipe E ON J.idE = E.idE
JOIN Classement C ON C.idC = E.idC
JOIN PARTICIPE P ON P.idJ = J.idJ
JOIN Phase Ph ON Ph.Niveau = P.Niveau
JOIN Tournoi ON T.idT = Ph.idT
WHERE (SELECT DISTINCT J.idJ
        FROM Joueur J
        JOIN Participe P ON P.idJ = J.idJ
        JOIN Phase Ph ON Ph.Niveau = P.Niveau
        JOIN Tournoi T ON T.idT = Ph.idT 
        ) f
;

// "les phases de tournoi (nom et année de tournoi, niveau de phase) jouées et pour lesquelles s’est qualifié
// l’utilisateur courant. Le résultat sera trié sur les années selon l’ordre antéchronologique puis sur les niveaux
// de phase selon l’ordre lexicographique inverse".
SELECT T.nom, Ph.Niveau
FROM Tournoi T
JOIN Phase Ph ON T.idT = Ph.idT
JOIN Participe P ON Ph.Niveau = P.Niveau
JOIN Joueur J ON P.idJ = J.idJ
WHERE P.est_qualifie = TRUE
ORDER BY T.dateDeb DESC, Ph.Niveau DESC
;

// "le nombre d’équipes classées premières des classements et dont aucun des membres n’est premier dans
// un classement individuel".
SELECT COUNT( DISTINCT E.idE )
FROM Equipe E
JOIN Joueur J ON J.idE = E.idE
JOIN EST_CLASSé EC ON J.idJ = EC.idJ
WHERE NOT EXISTS ( EC.rang=1 )
;

// "Pour les 3 dernières années, donner le nombre moyen de participants aux tournois".
SELECT AVG( DISTINCT J.idJ )
FROM Joueur J
JOIN Participe P ON P.idJ = J.idJ
JOIN Phase Ph ON P.Niveau = Ph.Niveau
JOIN Tournoi T ON T.idT = Ph.idT
LIMIT( T.DateDeb ) = 3

// "Donner le nom et le prénom des joueurs classés de manière individuelle dans le top 5 d’au moins 2
// classements de portée nationale".
SELECT J.nom, J.prénom
FROM Joueur J
WHERE J.idJ IN ( SELECT EC.idJ
                 FROM EST_CLASSE EC
                 JOIN Classement C ON C.idC = EC.idC
                 WHERE rang BETWEEN 1 AND 5 
                 AND C.portee='Nationale'
                 GROUP BY EC.idJ
                 HAVING COUNT( DISTINCT C.idC) >= 2
)
;


// "Pour chaque taille de plateau, donner le nombre de parties jouées avec un plateau de cette taille".
SELECT DISTINCT Pl.taille, COUNT( P.idP )
FROM Plateau Pl
JOIN CONTIENT C ON Pl.idPlateau = C.idPlateau
JOIN Partie P ON P.idP = C.idP
;

// "Le top 5 des joueurs (pseudo) qui ont joué le plus de parties"
SELECT J.pseudo
FROM Joueur J
JOIN J.idJ IN ( SELECT EC.idJ
                FROM EST_CLASSE EC
                ORDER BY EC.idJ
)
LIMIT () = 5
;