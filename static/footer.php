<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<footer>
	<a href="https://creativecommons.org/licenses/" target="_blank"><img src="img/by-nc-sa-eu.png" alt="Licence CC BY-NC-SA"/></a>
	<span><?php print date("Y"); ?> - Projet de développement web dans un cadre éducatif</span>
	<span><a href="https://forge.univ-lyon1.fr/lif-bdw/lifbdw.pages.univ-lyon1.fr/-/tree/main" target="_blank" alt="Page web de BDW">BDW - Base de données et programmation web - Partie TP</a> - <a href="https://www.univ-lyon1.fr/" target="_blank" alt ="page web de l'UCBL"> UCBL Lyon 1 </a></span>
</footer>
