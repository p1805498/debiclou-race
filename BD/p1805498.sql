-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 29 mars 2024 à 12:41
-- Version du serveur :  10.3.39-MariaDB-0ubuntu0.20.04.2
-- Version de PHP : 7.4.3-4ubuntu2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `p1805498`
--

-- --------------------------------------------------------

--
-- Structure de la table `A_VALIDE`
--

CREATE TABLE `A_VALIDE` (
  `idCo` varchar(50) NOT NULL,
  `couleur` varchar(50) NOT NULL,
  `valeur` varchar(50) NOT NULL,
  `rang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CARTE`
--

CREATE TABLE `CARTE` (
  `idC` varchar(50) NOT NULL,
  `niveau` varchar(50) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `points` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CHOISIT_MAIN`
--

CREATE TABLE `CHOISIT_MAIN` (
  `idJ` int(11) NOT NULL,
  `numT` varchar(50) NOT NULL,
  `nbDéRouges` int(11) DEFAULT NULL,
  `nbDéJaune` int(11) DEFAULT NULL,
  `nbDéBleu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CLASSEMENT`
--

CREATE TABLE `CLASSEMENT` (
  `idC` int(11) NOT NULL,
  `portee` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CLASSEMENT_INDIVIDUEL`
--

CREATE TABLE `CLASSEMENT_INDIVIDUEL` (
  `idC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CLASSEMENT_ÉQUIPE`
--

CREATE TABLE `CLASSEMENT_ÉQUIPE` (
  `idC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `COMPREND`
--

CREATE TABLE `COMPREND` (
  `numT` varchar(50) NOT NULL,
  `idL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `CONTIENT`
--

CREATE TABLE `CONTIENT` (
  `idPlateau` varchar(50) NOT NULL,
  `idP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Contrainte`
--

CREATE TABLE `Contrainte` (
  `idCo` varchar(50) NOT NULL,
  `couleur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `DES`
--

CREATE TABLE `DES` (
  `couleur` varchar(50) NOT NULL,
  `valeur` varchar(50) NOT NULL,
  `rang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `EFFECTUE`
--

CREATE TABLE `EFFECTUE` (
  `idJ` int(11) NOT NULL,
  `couleur` varchar(50) NOT NULL,
  `valeur` varchar(50) NOT NULL,
  `rang` varchar(50) NOT NULL,
  `idL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `EST_ASSOCIE_A`
--

CREATE TABLE `EST_ASSOCIE_A` (
  `idC` varchar(50) NOT NULL,
  `numT` varchar(50) NOT NULL,
  `idCo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `EST_CLASSÉ`
--

CREATE TABLE `EST_CLASSÉ` (
  `idJ` int(11) NOT NULL,
  `idC` int(11) NOT NULL,
  `rang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `EST_COMPOSE`
--

CREATE TABLE `EST_COMPOSE` (
  `idC` varchar(50) NOT NULL,
  `idPlateau` varchar(50) NOT NULL,
  `rang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `EST_LIÉ_À`
--

CREATE TABLE `EST_LIÉ_À` (
  `idC` int(11) NOT NULL,
  `idC_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `FACE_DE_DE`
--

CREATE TABLE `FACE_DE_DE` (
  `idCo` varchar(50) NOT NULL,
  `valeur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `HISTORIQUE`
--

CREATE TABLE `HISTORIQUE` (
  `idJ` int(11) NOT NULL,
  `idC` varchar(50) NOT NULL,
  `idP` int(11) NOT NULL,
  `joue_act` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `JOUE`
--

CREATE TABLE `JOUE` (
  `idJ` int(11) NOT NULL,
  `idP` int(11) NOT NULL,
  `ordre` int(11) DEFAULT NULL,
  `score` varchar(50) DEFAULT NULL,
  `couleur_pion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `JOUEUR`
--

CREATE TABLE `JOUEUR` (
  `idJ` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prénom` varchar(50) DEFAULT NULL,
  `pseudo` varchar(50) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `idE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `LANCER`
--

CREATE TABLE `LANCER` (
  `idL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `MEME_AU_CHOIX`
--

CREATE TABLE `MEME_AU_CHOIX` (
  `idCo` varchar(50) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `PARTICIPE`
--

CREATE TABLE `PARTICIPE` (
  `idJ` int(11) NOT NULL,
  `Niveau` varchar(50) NOT NULL,
  `a_joue` tinyint(1) DEFAULT NULL,
  `est_qualifie` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Partie`
--

CREATE TABLE `Partie` (
  `idP` int(11) NOT NULL,
  `dateP` date DEFAULT NULL,
  `horaire` time DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  `nbJoueurs` int(11) DEFAULT NULL,
  `mode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `PHASE`
--

CREATE TABLE `PHASE` (
  `Niveau` varchar(50) NOT NULL,
  `dateP` date DEFAULT NULL,
  `idT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Plateau`
--

CREATE TABLE `Plateau` (
  `idPlateau` varchar(50) NOT NULL,
  `taille` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `SERIE_AU_CHOIX`
--

CREATE TABLE `SERIE_AU_CHOIX` (
  `idCo` varchar(50) NOT NULL,
  `nombre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `SEUIL_DE_DES`
--

CREATE TABLE `SEUIL_DE_DES` (
  `idCo` varchar(50) NOT NULL,
  `valeur` int(11) DEFAULT NULL,
  `sens` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `SE_DECOMPOSE`
--

CREATE TABLE `SE_DECOMPOSE` (
  `numT` varchar(50) NOT NULL,
  `idP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `SONT_CLASSÉS`
--

CREATE TABLE `SONT_CLASSÉS` (
  `idE` int(11) NOT NULL,
  `idC` int(11) NOT NULL,
  `rang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `TENTE_VALIDATION`
--

CREATE TABLE `TENTE_VALIDATION` (
  `idJ` int(11) NOT NULL,
  `idC` varchar(50) NOT NULL,
  `numT` varchar(50) NOT NULL,
  `nb_tentatives` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `TOUR`
--

CREATE TABLE `TOUR` (
  `numT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `TOURNOI`
--

CREATE TABLE `TOURNOI` (
  `idT` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `dateDeb` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ÉQUIPE`
--

CREATE TABLE `ÉQUIPE` (
  `idE` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `A_VALIDE`
--
ALTER TABLE `A_VALIDE`
  ADD PRIMARY KEY (`idCo`,`couleur`,`valeur`,`rang`),
  ADD KEY `couleur` (`couleur`,`valeur`,`rang`);

--
-- Index pour la table `CARTE`
--
ALTER TABLE `CARTE`
  ADD PRIMARY KEY (`idC`);

--
-- Index pour la table `CHOISIT_MAIN`
--
ALTER TABLE `CHOISIT_MAIN`
  ADD PRIMARY KEY (`idJ`,`numT`),
  ADD KEY `numT` (`numT`);

--
-- Index pour la table `CLASSEMENT`
--
ALTER TABLE `CLASSEMENT`
  ADD PRIMARY KEY (`idC`);

--
-- Index pour la table `CLASSEMENT_INDIVIDUEL`
--
ALTER TABLE `CLASSEMENT_INDIVIDUEL`
  ADD PRIMARY KEY (`idC`);

--
-- Index pour la table `CLASSEMENT_ÉQUIPE`
--
ALTER TABLE `CLASSEMENT_ÉQUIPE`
  ADD PRIMARY KEY (`idC`);

--
-- Index pour la table `COMPREND`
--
ALTER TABLE `COMPREND`
  ADD PRIMARY KEY (`numT`,`idL`),
  ADD KEY `idL` (`idL`);

--
-- Index pour la table `CONTIENT`
--
ALTER TABLE `CONTIENT`
  ADD PRIMARY KEY (`idPlateau`,`idP`),
  ADD KEY `idP` (`idP`);

--
-- Index pour la table `Contrainte`
--
ALTER TABLE `Contrainte`
  ADD PRIMARY KEY (`idCo`);

--
-- Index pour la table `DES`
--
ALTER TABLE `DES`
  ADD PRIMARY KEY (`couleur`,`valeur`,`rang`);

--
-- Index pour la table `EFFECTUE`
--
ALTER TABLE `EFFECTUE`
  ADD PRIMARY KEY (`idJ`,`couleur`,`valeur`,`rang`,`idL`),
  ADD KEY `couleur` (`couleur`,`valeur`,`rang`),
  ADD KEY `idL` (`idL`);

--
-- Index pour la table `EST_ASSOCIE_A`
--
ALTER TABLE `EST_ASSOCIE_A`
  ADD PRIMARY KEY (`idC`,`numT`,`idCo`),
  ADD KEY `numT` (`numT`),
  ADD KEY `idCo` (`idCo`);

--
-- Index pour la table `EST_CLASSÉ`
--
ALTER TABLE `EST_CLASSÉ`
  ADD PRIMARY KEY (`idJ`,`idC`),
  ADD KEY `idC` (`idC`);

--
-- Index pour la table `EST_COMPOSE`
--
ALTER TABLE `EST_COMPOSE`
  ADD PRIMARY KEY (`idC`,`idPlateau`),
  ADD KEY `idPlateau` (`idPlateau`);

--
-- Index pour la table `EST_LIÉ_À`
--
ALTER TABLE `EST_LIÉ_À`
  ADD PRIMARY KEY (`idC`,`idC_1`),
  ADD KEY `idC_1` (`idC_1`);

--
-- Index pour la table `FACE_DE_DE`
--
ALTER TABLE `FACE_DE_DE`
  ADD PRIMARY KEY (`idCo`);

--
-- Index pour la table `HISTORIQUE`
--
ALTER TABLE `HISTORIQUE`
  ADD PRIMARY KEY (`idJ`,`idC`,`idP`),
  ADD KEY `idC` (`idC`),
  ADD KEY `idP` (`idP`);

--
-- Index pour la table `JOUE`
--
ALTER TABLE `JOUE`
  ADD PRIMARY KEY (`idJ`,`idP`),
  ADD KEY `idP` (`idP`);

--
-- Index pour la table `JOUEUR`
--
ALTER TABLE `JOUEUR`
  ADD PRIMARY KEY (`idJ`),
  ADD KEY `idE` (`idE`);

--
-- Index pour la table `LANCER`
--
ALTER TABLE `LANCER`
  ADD PRIMARY KEY (`idL`);

--
-- Index pour la table `MEME_AU_CHOIX`
--
ALTER TABLE `MEME_AU_CHOIX`
  ADD PRIMARY KEY (`idCo`);

--
-- Index pour la table `PARTICIPE`
--
ALTER TABLE `PARTICIPE`
  ADD PRIMARY KEY (`idJ`,`Niveau`),
  ADD KEY `Niveau` (`Niveau`);

--
-- Index pour la table `Partie`
--
ALTER TABLE `Partie`
  ADD PRIMARY KEY (`idP`);

--
-- Index pour la table `PHASE`
--
ALTER TABLE `PHASE`
  ADD PRIMARY KEY (`Niveau`),
  ADD KEY `idT` (`idT`);

--
-- Index pour la table `Plateau`
--
ALTER TABLE `Plateau`
  ADD PRIMARY KEY (`idPlateau`);

--
-- Index pour la table `SERIE_AU_CHOIX`
--
ALTER TABLE `SERIE_AU_CHOIX`
  ADD PRIMARY KEY (`idCo`);

--
-- Index pour la table `SEUIL_DE_DES`
--
ALTER TABLE `SEUIL_DE_DES`
  ADD PRIMARY KEY (`idCo`);

--
-- Index pour la table `SE_DECOMPOSE`
--
ALTER TABLE `SE_DECOMPOSE`
  ADD PRIMARY KEY (`numT`,`idP`),
  ADD KEY `idP` (`idP`);

--
-- Index pour la table `SONT_CLASSÉS`
--
ALTER TABLE `SONT_CLASSÉS`
  ADD PRIMARY KEY (`idE`,`idC`),
  ADD KEY `idC` (`idC`);

--
-- Index pour la table `TENTE_VALIDATION`
--
ALTER TABLE `TENTE_VALIDATION`
  ADD PRIMARY KEY (`idJ`,`idC`,`numT`),
  ADD KEY `idC` (`idC`),
  ADD KEY `numT` (`numT`);

--
-- Index pour la table `TOUR`
--
ALTER TABLE `TOUR`
  ADD PRIMARY KEY (`numT`);

--
-- Index pour la table `TOURNOI`
--
ALTER TABLE `TOURNOI`
  ADD PRIMARY KEY (`idT`);

--
-- Index pour la table `ÉQUIPE`
--
ALTER TABLE `ÉQUIPE`
  ADD PRIMARY KEY (`idE`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `A_VALIDE`
--
ALTER TABLE `A_VALIDE`
  ADD CONSTRAINT `A_VALIDE_ibfk_1` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`),
  ADD CONSTRAINT `A_VALIDE_ibfk_2` FOREIGN KEY (`couleur`,`valeur`,`rang`) REFERENCES `DES` (`couleur`, `valeur`, `rang`);

--
-- Contraintes pour la table `CHOISIT_MAIN`
--
ALTER TABLE `CHOISIT_MAIN`
  ADD CONSTRAINT `CHOISIT_MAIN_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `CHOISIT_MAIN_ibfk_2` FOREIGN KEY (`numT`) REFERENCES `TOUR` (`numT`);

--
-- Contraintes pour la table `CLASSEMENT_INDIVIDUEL`
--
ALTER TABLE `CLASSEMENT_INDIVIDUEL`
  ADD CONSTRAINT `CLASSEMENT_INDIVIDUEL_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `CLASSEMENT` (`idC`);

--
-- Contraintes pour la table `CLASSEMENT_ÉQUIPE`
--
ALTER TABLE `CLASSEMENT_ÉQUIPE`
  ADD CONSTRAINT `CLASSEMENT_ÉQUIPE_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `CLASSEMENT` (`idC`);

--
-- Contraintes pour la table `COMPREND`
--
ALTER TABLE `COMPREND`
  ADD CONSTRAINT `COMPREND_ibfk_1` FOREIGN KEY (`numT`) REFERENCES `TOUR` (`numT`),
  ADD CONSTRAINT `COMPREND_ibfk_2` FOREIGN KEY (`idL`) REFERENCES `LANCER` (`idL`);

--
-- Contraintes pour la table `CONTIENT`
--
ALTER TABLE `CONTIENT`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idPlateau`) REFERENCES `Plateau` (`idPlateau`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idP`) REFERENCES `Partie` (`idP`);

--
-- Contraintes pour la table `EFFECTUE`
--
ALTER TABLE `EFFECTUE`
  ADD CONSTRAINT `EFFECTUE_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `EFFECTUE_ibfk_2` FOREIGN KEY (`couleur`,`valeur`,`rang`) REFERENCES `DES` (`couleur`, `valeur`, `rang`),
  ADD CONSTRAINT `EFFECTUE_ibfk_3` FOREIGN KEY (`idL`) REFERENCES `LANCER` (`idL`);

--
-- Contraintes pour la table `EST_ASSOCIE_A`
--
ALTER TABLE `EST_ASSOCIE_A`
  ADD CONSTRAINT `EST_ASSOCIE_A_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `CARTE` (`idC`),
  ADD CONSTRAINT `EST_ASSOCIE_A_ibfk_2` FOREIGN KEY (`numT`) REFERENCES `TOUR` (`numT`),
  ADD CONSTRAINT `EST_ASSOCIE_A_ibfk_3` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`);

--
-- Contraintes pour la table `EST_CLASSÉ`
--
ALTER TABLE `EST_CLASSÉ`
  ADD CONSTRAINT `EST_CLASSÉ_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `EST_CLASSÉ_ibfk_2` FOREIGN KEY (`idC`) REFERENCES `CLASSEMENT_INDIVIDUEL` (`idC`);

--
-- Contraintes pour la table `EST_COMPOSE`
--
ALTER TABLE `EST_COMPOSE`
  ADD CONSTRAINT `EST_COMPOSE_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `CARTE` (`idC`),
  ADD CONSTRAINT `EST_COMPOSE_ibfk_2` FOREIGN KEY (`idPlateau`) REFERENCES `Plateau` (`idPlateau`);

--
-- Contraintes pour la table `EST_LIÉ_À`
--
ALTER TABLE `EST_LIÉ_À`
  ADD CONSTRAINT `EST_LIÉ_À_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `CLASSEMENT` (`idC`),
  ADD CONSTRAINT `EST_LIÉ_À_ibfk_2` FOREIGN KEY (`idC_1`) REFERENCES `CLASSEMENT` (`idC`);

--
-- Contraintes pour la table `FACE_DE_DE`
--
ALTER TABLE `FACE_DE_DE`
  ADD CONSTRAINT `FACE_DE_DE_ibfk_1` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`);

--
-- Contraintes pour la table `HISTORIQUE`
--
ALTER TABLE `HISTORIQUE`
  ADD CONSTRAINT `HISTORIQUE_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `HISTORIQUE_ibfk_2` FOREIGN KEY (`idC`) REFERENCES `CARTE` (`idC`),
  ADD CONSTRAINT `HISTORIQUE_ibfk_3` FOREIGN KEY (`idP`) REFERENCES `Partie` (`idP`);

--
-- Contraintes pour la table `JOUE`
--
ALTER TABLE `JOUE`
  ADD CONSTRAINT `JOUE_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `JOUE_ibfk_2` FOREIGN KEY (`idP`) REFERENCES `Partie` (`idP`);

--
-- Contraintes pour la table `JOUEUR`
--
ALTER TABLE `JOUEUR`
  ADD CONSTRAINT `JOUEUR_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `ÉQUIPE` (`idE`);

--
-- Contraintes pour la table `MEME_AU_CHOIX`
--
ALTER TABLE `MEME_AU_CHOIX`
  ADD CONSTRAINT `MEME_AU_CHOIX_ibfk_1` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`);

--
-- Contraintes pour la table `PARTICIPE`
--
ALTER TABLE `PARTICIPE`
  ADD CONSTRAINT `PARTICIPE_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `PARTICIPE_ibfk_2` FOREIGN KEY (`Niveau`) REFERENCES `PHASE` (`Niveau`);

--
-- Contraintes pour la table `PHASE`
--
ALTER TABLE `PHASE`
  ADD CONSTRAINT `PHASE_ibfk_1` FOREIGN KEY (`idT`) REFERENCES `TOURNOI` (`idT`);

--
-- Contraintes pour la table `SERIE_AU_CHOIX`
--
ALTER TABLE `SERIE_AU_CHOIX`
  ADD CONSTRAINT `SERIE_AU_CHOIX_ibfk_1` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`);

--
-- Contraintes pour la table `SEUIL_DE_DES`
--
ALTER TABLE `SEUIL_DE_DES`
  ADD CONSTRAINT `SEUIL_DE_DES_ibfk_1` FOREIGN KEY (`idCo`) REFERENCES `Contrainte` (`idCo`);

--
-- Contraintes pour la table `SE_DECOMPOSE`
--
ALTER TABLE `SE_DECOMPOSE`
  ADD CONSTRAINT `SE_DECOMPOSE_ibfk_1` FOREIGN KEY (`numT`) REFERENCES `TOUR` (`numT`),
  ADD CONSTRAINT `SE_DECOMPOSE_ibfk_2` FOREIGN KEY (`idP`) REFERENCES `Partie` (`idP`);

--
-- Contraintes pour la table `SONT_CLASSÉS`
--
ALTER TABLE `SONT_CLASSÉS`
  ADD CONSTRAINT `SONT_CLASSÉS_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `ÉQUIPE` (`idE`),
  ADD CONSTRAINT `SONT_CLASSÉS_ibfk_2` FOREIGN KEY (`idC`) REFERENCES `CLASSEMENT_ÉQUIPE` (`idC`);

--
-- Contraintes pour la table `TENTE_VALIDATION`
--
ALTER TABLE `TENTE_VALIDATION`
  ADD CONSTRAINT `TENTE_VALIDATION_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `JOUEUR` (`idJ`),
  ADD CONSTRAINT `TENTE_VALIDATION_ibfk_2` FOREIGN KEY (`idC`) REFERENCES `CARTE` (`idC`),
  ADD CONSTRAINT `TENTE_VALIDATION_ibfk_3` FOREIGN KEY (`numT`) REFERENCES `TOUR` (`numT`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
