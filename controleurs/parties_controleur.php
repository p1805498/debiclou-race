<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php 

$message_liste = "";
$message_details = "";

/***********************************/
/* Gestion de la liste des tables */
/***********************************/

$tables = get_tables();

if($tables == null || count($tables) == 0) {
	$message_liste = "Aucune table n'a été trouvée dans la base de données !";
}


/***********************************/
/* Gestion du détail d'une table   */
/***********************************/

	//$type_vue = mysqli_real_escape_string($connexion, trim($_POST['typeVue']));
	//$nom_table = mysqli_real_escape_string($connexion, trim($_POST['nomTable']));
	
	$inst = 'data';

	$affParties = mysqli_real_escape_string($connexion, trim('Partie'));

	$resultats = get_infos($inst, $affParties);
	if($resultats == null){
		$message_details = "Aucune information disponible sur $inst de $affParties !";
	}else{
		if( count($resultats) == 0 ){
			$message_details = "La table $nom_table est vide!";
		}
	}

?>
