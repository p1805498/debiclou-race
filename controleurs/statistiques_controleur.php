<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php

// Fonction récupérant toute une série de statistique et les affichant
function nombreEntitee() {
    $nbJoueur = executer_une_requete("SELECT COUNT(DISTINCT idJ) FROM `JOUEUR`");
    $nbJoueur = $nbJoueur[0]['COUNT(DISTINCT idJ)'];

    $nbEquipe = executer_une_requete("SELECT COUNT(DISTINCT idE) FROM `ÉQUIPE` ");
    $nbEquipe = $nbEquipe[0]['COUNT(DISTINCT idE)'];

    $nbClassement = executer_une_requete("SELECT COUNT(DISTINCT idC) FROM `CLASSEMENT`");
    $nbClassement = $nbClassement[0]['COUNT(DISTINCT idC)'];

    $nbTournoi = executer_une_requete("SELECT COUNT(DISTINCT idT) FROM `TOURNOI`");
    $nbTournoi = $nbTournoi[0]['COUNT(DISTINCT idT)'];

    $moyTournoi = executer_une_requete("SELECT ROUND(AVG(moyenne_participants), 1) AS moyenne_generale FROM ( SELECT AVG(nombre_participants) AS moyenne_participants FROM ( SELECT idT, COUNT(idJ) AS nombre_participants FROM PARTICIPE GROUP BY idT ) AS participants_par_tournoi ) AS moyenne_par_tournoi");
    $moyTournoi = $moyTournoi[0]['moyenne_generale'];

    $firstTeam = executer_une_requete("SELECT COUNT(DISTINCT E.idE) FROM ÉQUIPE E JOIN JOUEUR J ON J.idE = E.idE JOIN CLASSEMENT_INDIVIDUEL CI ON J.idJ = CI.idJ JOIN CLASSEMENT_ÉQUIPE CE ON CE.idE = E.idE WHERE CI.rang != 1 AND CE.rang = 1 ");
    $firstTeam = $firstTeam[0]['COUNT(DISTINCT E.idE)'];

    $topPlayer = executer_une_requete("SELECT J.nom, J.prénom FROM JOUEUR J WHERE J.idJ IN ( SELECT EC.idJ FROM EST_CLASSÉ EC JOIN CLASSEMENT C ON C.idC = EC.idC WHERE rang BETWEEN 1 AND 5 AND C.portee='Nationale' GROUP BY EC.idJ HAVING COUNT( DISTINCT C.idC) >= 2 ) ");

    $nbPartie = executer_une_requete("SELECT Pl.taille, COUNT(P.idP) AS nombre_de_parties FROM Plateau Pl JOIN CONTIENT C ON Pl.idPlateau = C.idPlateau JOIN Partie P ON P.idP = C.idP GROUP BY Pl.taille ");

    $topFive = executer_une_requete("SELECT JR.pseudo, COUNT(*) AS nombre_de_parties_jouees FROM JOUE J JOIN JOUEUR JR ON J.idJ = JR.idJ GROUP BY JR.idJ ORDER BY nombre_de_parties_jouees DESC LIMIT 5");


    echo("<p class=\"bloc_commandes\">Nombre de joueur : $nbJoueur<br>
        Nombre d'équipe : $nbEquipe<br>
        Nombre de classement : $nbClassement<br>
        Nombre de tournoi : $nbTournoi<br>
        Moyenne de participants par tournoi : $moyTournoi<br>
        Nombre d'équipes classées premières des classements et dont aucun des membres n'est premier dans un classement individuel : $firstTeam<br><br>
        
        </p><br>");

    echo("<p class=\"bloc_commandes\">Joueurs classés de manière individuelle dans le top 5 d'au moins 2 classements de portée nationale : <br>");
    foreach ($topPlayer as $row) {
        echo "- " . $row['nom'] . " " . $row['prénom'] . "<br>";
    }
    echo("</p><br>");

    echo("<p class=\"bloc_commandes\">Nombre de parties jouées avec un plateau de chaque taille : <br>");
    foreach ($nbPartie as $row) {
        echo "- " . $row['taille'] . " : " . $row['nombre_de_parties'] . "<br>";
    }
    echo("</p><br>");

    echo("<p class=\"bloc_commandes\">Le top 5 des joueurs (pseudo) qui ont joué le plus de parties : <br>");
    foreach ($topFive as $row) {
        echo "- " . $row['pseudo'] . " : " . $row['nombre_de_parties_jouees'] . "<br>";
    }
    echo("</p><br>");
}

// Fonction particulière pour les phases de tournois concernant l'utilisateur courant
function phaseTournoi() {
    $idJoueur = $_SESSION['idUser'];

    $phaseTournoi = executer_une_requete("SELECT T.nom, YEAR(T.dateFin) AS Annee, P.Niveau FROM TOURNOI T JOIN PARTICIPE P ON T.idT = P.idT JOIN JOUEUR J ON P.idJ = J.idJ WHERE P.idJ = $idJoueur ORDER BY T.dateDeb DESC, P.Niveau DESC ");
    
    echo("<p>Phases de tournoi jouées et pour lesquelles vous vous êtes qualifié : ");

    echo "<table border='1'>";
    echo "<tr><th>Nom du tournoi</th><th>Année</th><th>Niveau de la phase</th></tr>";
    
    foreach ($phaseTournoi as $row) {
        echo "<tr><td>" . $row['nom'] . "</td><td>" . $row['Annee'] . "</td><td>" . $row['Niveau'] . "</td></tr></p>";
    }
    
    echo "</table>";   
}

?>



