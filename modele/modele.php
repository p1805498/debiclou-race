<!--BOUARROUDJ Nizar, VANDEN BORRE Léon - PROJET DE CRÉATION DE SITE WEB - UE LIFBDW - UCB LYON 1-->
<?php 

////////////////////////////////////////////////////////////////////////
///////    Gestion de la connxeion   ///////////////////////////////////
////////////////////////////////////////////////////////////////////////

/**
 * Initialise la connexion à la base de données courante (spécifiée selon constante 
 *	globale SERVEUR, UTILISATEUR, MOTDEPASSE, BDD)			
 */
function open_connection_DB() {
	global $connexion;

	$connexion = mysqli_connect(SERVEUR, UTILISATEUR, MOTDEPASSE, BDD);
	if (mysqli_connect_errno()) {
	    printf("Échec de la connexion : %s\n", mysqli_connect_error());
	    exit();
	}
}

/**
 *  	Ferme la connexion courante
 * */
function close_connection_DB() {
	global $connexion;

	mysqli_close($connexion);
}


////////////////////////////////////////////////////////////////////////
///////   Accès au dictionnaire       ///////////////////////////////////
////////////////////////////////////////////////////////////////////////


/**
 *  Retourne la liste des tables définies dans la base de données courantes (BDD)
 * */
function get_tables() {
	global $connexion;

	$requete = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '". BDD ."'";

	$res = mysqli_query($connexion, $requete);
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	return $instances;
}



/**
 *  Retourne les statistiques sur la base de données courante
 * */
function get_statistiques() {

	return null;
}

////////////////////////////////////////////////////////////////////////
///////    Informations (structure et contenu) d'une table    //////////
////////////////////////////////////////////////////////////////////////

/**
 *  Retourne le détail des infos sur une table
 * */
function get_infos( $typeVue, $nomTable ) {
	global $connexion;

	switch ( $typeVue) {
		case 'schema': return get_infos_schema( $nomTable ); break;
		case 'data': return get_infos_instances( $nomTable ); break;
		default: return null; 
	}
}

/**
 * Retourne le détail sur le schéma de la table
*/
function get_infos_schema( $nomTable ) {
	global $connexion;

	// récupération des informations sur la table (schema + instance)
	$requete = "SELECT * FROM $nomTable";
	$res = mysqli_query($connexion, $requete);

	// construction du schéma qui sera composé du nom de l'attribut et de son type	
	$schema = array( array( 'nom' => 'nom_attribut' ), array( 'nom' => 'type_attribut' ) , array('nom' => 'clé')) ;

	// récupération des valeurs associées au nom et au type des attributs
	$metadonnees = mysqli_fetch_fields($res);

	$infos_att = array();
	foreach( $metadonnees as $att ){
		//var_dump($att);

 		$is_in_pk = ($att->flags & MYSQLI_PRI_KEY_FLAG)?'PK':'';
 		$type = convertir_type($att->{'type'});

		array_push( $infos_att , array( 'nom' => $att->{'name'}, 'type' => $type , 'cle' => $is_in_pk) );	
	}

	return array('schema'=> $schema , 'instances'=> $infos_att);

}

/**
 * Retourne les instances de la table
*/
function get_infos_instances( $nomTable ) {
	global $connexion;

	// récupération des informations sur la table (schema + instance)
	$requete = "SELECT * FROM $nomTable";  
 	$res = mysqli_query($connexion, $requete);  

 	// extraction des informations sur le schéma à partir du résultat précédent
	$infos_atts = mysqli_fetch_fields($res); 

	// filtrage des information du schéma pour ne garder que le nom de l'attribut
	$schema = array();
	foreach( $infos_atts as $att ){
		array_push( $schema , array( 'nom' => $att->{'name'} ) ); // syntaxe objet permettant de récupérer la propriété 'name' du de l'objet descriptif de l'attribut courant
	}

	// récupération des données (instances) de la table
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);

	// renvoi d'un tableau contenant les informations sur le schéma (nom d'attribut) et les n-uplets
	return array('schema'=> $schema , 'instances'=> $instances);

}


function convertir_type( $code ){
	switch( $code ){
		case 1 : return 'BOOL/TINYINT';
		case 2 : return 'SMALLINT';
		case 3 : return 'INTEGER';
		case 4 : return 'FLOAT';
		case 5 : return 'DOUBLE';
		case 7 : return 'TIMESTAMP';
		case 8 : return 'BIGINT/SERIAL';
		case 9 : return 'MEDIUMINT';
		case 10 : return 'DATE';
		case 11 : return 'TIME';
		case 12 : return 'DATETIME';
		case 13 : return 'YEAR';
		case 16 : return 'BIT';
		case 246 : return 'DECIMAL/NUMERIC/FIXED';
		case 252 : return 'BLOB/TEXT';
		case 253 : return 'VARCHAR/VARBINARY';
		case 254 : return 'CHAR/SET/ENUM/BINARY';
		default : return '?';
	}

}

////////////////////////////////////////////////////////////////////////
///////    Traitement de requêtes                             //////////
////////////////////////////////////////////////////////////////////////

/**
 * Retourne le résultat (schéma et instances) de la requ$ete $requete
 * */
function executer_une_requete($requete) {
    global $connexion;

    // Exécution de la requête SQL
    $res = mysqli_query($connexion, $requete);

    // Vérification si la requête a réussi
    if ($res === false) {
        // Affichage de l'erreur MySQL, si elle existe
        echo "Erreur MySQL : " . mysqli_error($connexion);
        return null;
    }

    // Récupération des résultats de la requête
    $instances = mysqli_fetch_all($res, MYSQLI_ASSOC);

    // Libération de la mémoire utilisée par le résultat de la requête
    mysqli_free_result($res);

    // Retourne les résultats
    return $instances;
}

function ordrePassageJoueurs($modeJeu, $indices_joueurs) {
    switch ($modeJeu) {
        case "ALEA": // Mode aléatoire
            shuffle($indices_joueurs); // Mélange aléatoire des identifiants des joueurs
            break;
        case "+JEUNE": // Mode "+JEUNE" - ordre de passage du plus jeune au plus vieux
            sort($indices_joueurs); // Trie les identifiants des joueurs par ordre croissant
            break;
        case "-EXPE": // Mode "-EXPE" - ordre de passage du plus expérimenté au moins expérimenté
            rsort($indices_joueurs); // Trie les identifiants des joueurs par ordre décroissant
            break;
        default:
            // Gestion d'un mode non reconnu ou par défaut
            // Vous pouvez définir un comportement par défaut ici
            break;
    }
    return $indices_joueurs;
}

// Fonction pour trouver le prochain joueur
function prochainJoueur($ordreJoueurs, $idJoueurActuel) {
    // Recherche de l'index du joueur actuel dans le tableau
    $indexActuel = array_search($idJoueurActuel, $ordreJoueurs);

    // Si le joueur actuel n'est pas trouvé ou s'il est le dernier dans l'ordre
    if ($indexActuel === false || $indexActuel === count($ordreJoueurs) - 1) {
        // Retourne le premier joueur dans l'ordre
        return $ordreJoueurs[0];
    } else {
        // Retourne l'ID du joueur suivant dans l'ordre
        return $ordreJoueurs[$indexActuel + 1];
    }
}

// Fonction pour initialiser les positions des joueurs
function initialiser_positions($indice_joueur) {
    $positions = array();
    foreach ($indice_joueur as $identifiant) {
        $positions[$identifiant] = 0;
    }
    return $positions;
}

// Fonction pour retrouver l'ID du joueur à partir de la position
function retrouver_id_joueur($position) {
    if(isset($_SESSION['position'])) {
        foreach($_SESSION['position'] as $idJoueur => $pos) {
            if($pos == $position) {
                return $idJoueur;
            }
        }
    }
    return null; // Retourne null si aucune correspondance trouvée
}

// Fonction pour mettre à jour la position d'un joueur
function mettre_a_jour_position($idJoueur, $nouvelle_position) {
    if (isset($_SESSION['position'][$idJoueur])) {
        $_SESSION['position'][$idJoueur] = $nouvelle_position;
    } else {
        echo "Erreur : ID du joueur non valide.";
    }
    /*
    // Retrouver l'ID du joueur à partir de la nouvelle position
    $joueur_id = retrouver_id_joueur($nouvelle_position);
    if($joueur_id !== null) {
        echo "L'ID du joueur associé à la position $nouvelle_position est : $joueur_id";
    } else {
        echo "Aucun joueur trouvé pour la position $nouvelle_position";
    }*/
}

function determiner_position($nbCP, $nbDesR, $nbDesB, $nbDesJ) {
    // Initialisation de la position à la case de départ (0)
    $position = 0;

    // Récupération des cartes attribuées par le formulaire
    $cartes_attribuees = array();
    for ($i = 1; $i <= $nbDesR; $i++) {
        if (isset($_POST["CTT_$i"])) {
            $cartes_attribuees[] = $_POST["CTT_$i"];
        }
    }
    for ($i = 1; $i <= $nbDesB; $i++) {
        if (isset($_POST["CTT_$i"])) {
            $cartes_attribuees[] = $_POST["CTT_$i"];
        }
    }
    for ($i = 1; $i <= $nbDesJ; $i++) {
        if (isset($_POST["CTT_$i"])) {
            $cartes_attribuees[] = $_POST["CTT_$i"];
        }
    }

    // Tri des cartes attribuées pour déterminer la carte d'arrivée
    sort($cartes_attribuees);

    // Ajout de 1 pour prendre en compte la case d'arrivée
    $position += count($cartes_attribuees) + 1;

    return $position;
}

// Déterminer l'indice du joueur à partir de son idJ
function trouver_indice_joueur($idJ) {
    if (isset($_SESSION['indice_joueur'])) {
        // Parcourir le tableau des indices des joueurs
        foreach ($_SESSION['indice_joueur'] as $indice => $idJoueur) {
            // Vérifier si l'idJ correspond à celui recherché
            if ($idJoueur === $idJ) {
                // Retourner l'indice du joueur
                return $indice;
            }
        }
    }
    // Si l'idJ n'est pas trouvé, retourner null ou une valeur par défaut
    return null;
}

// Fonction pour récupérer l'indice du joueur en fonction de la position de la colonne
function getJoueurIndice($position, $positions_pions) {
    foreach ($positions_pions as $idJoueur => $pos) {
        if ($pos == $position) {
            return $idJoueur;
        }
    }
    return null; // Retourner null si aucun joueur trouvé à cette position
}

function verifierNombreDes($nbDesRouges, $nbDesBleus, $nbDesJaunes, $nbDesRmax, $nbDesBmax, $nbDesJmax) {
    // Calculer le nombre total de dés saisis
    $totalDes = $nbDesRouges + $nbDesBleus + $nbDesJaunes;
    
    // Vérifier si le nombre total de dés dépasse le maximum autorisé
    if ($totalDes > ($nbDesRmax + $nbDesBmax + $nbDesJmax)) {
        return false; // Le nombre total de dés dépasse le maximum autorisé
    } else {
        return true; // Le nombre total de dés est conforme aux limites
    }
}

function tirageCartes($nbCV, $nbCO, $nbCN) {
    $deck = array();

    // Tirage aléatoire des cartes vertes
    for ($i = 0; $i < $nbCV; $i++) {
        $carteVerte = rand(1, 25);
        $deck[] = array('idC' => $carteVerte, 'niveau' => 'V', 'points' => 2);
    }

    // Tirage aléatoire des cartes oranges
    for ($i = 0; $i < $nbCO; $i++) {
        $carteOrange = rand(26, 50);
        $deck[] = array('idC' => $carteOrange, 'niveau' => 'O', 'points' => 4);
    }

    // Tirage aléatoire des cartes noires
    for ($i = 0; $i < $nbCN; $i++) {
        $carteNoire = rand(51, 75);
        $deck[] = array('idC' => $carteNoire, 'niveau' => 'N', 'points' => 6);
    }

    return $deck;
}

function tirerCartes($nbCartes) {
	$deck = array();
	$imagesC = range(1, 75); // Créer un tableau avec les numéros des cartes de 1 à 75
	shuffle($imagesC); // Mélanger les numéros des cartes

	// Sélectionner les premières $nbCartes cartes mélangées
	for ($i = 0; $i < $nbCartes; $i++) {
		$deck[] = array('idC' => $imagesC[$i]); // Ajouter les cartes sélectionnées au deck
	}

	return $deck; // Retourner le deck de cartes tirées au sort
}

?>
